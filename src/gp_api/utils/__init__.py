from .hypercube import sample_hypercube
from .fit_compact import fit_compact_nd, train_function
from .fit_matern import fit_matern_nd
from .multigauss import Multigauss
