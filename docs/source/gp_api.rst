gp\_api package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   gp_api.kernels
   gp_api.utils

Submodules
----------

gp\_api.gaussian\_process module
--------------------------------

.. automodule:: gp_api.gaussian_process
   :members:
   :undoc-members:
   :show-inheritance:

gp\_api.marginals module
------------------------

.. automodule:: gp_api.marginals
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gp_api
   :members:
   :undoc-members:
   :show-inheritance:
