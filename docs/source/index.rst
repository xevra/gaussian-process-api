.. Gaussian Process API documentation master file, created by
   sphinx-quickstart on Tue Apr  5 16:04:03 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Gaussian Process API
====================

Gaussian Process API provides efficient Python implementations of several
flavors of
`Gaussian processes <https://en.wikipedia.org/wiki/Gaussian_process>`_.


Basic Usage
-----------

.. todo:: write a very simple example to demonstrate the purpose

To learn more, check out the :ref:`quickstart` tutorial.


.. toctree::
   :maxdepth: 1
   :caption: User Guide

   user/install
   modules


.. toctree::
   :maxdepth: 1
   :caption: Tutorials

   tutorials/quickstart
   tutorials/fitting
   tutorials/serialization
   tutorials/marginals
