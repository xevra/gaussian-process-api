gp\_api.utils package
=====================

Submodules
----------

gp\_api.utils.fit\_compact module
---------------------------------

.. automodule:: gp_api.utils.fit_compact
   :members:
   :undoc-members:
   :show-inheritance:

gp\_api.utils.fit\_matern module
--------------------------------

.. automodule:: gp_api.utils.fit_matern
   :members:
   :undoc-members:
   :show-inheritance:

gp\_api.utils.hypercube module
------------------------------

.. automodule:: gp_api.utils.hypercube
   :members:
   :undoc-members:
   :show-inheritance:

gp\_api.utils.multigauss module
-------------------------------

.. automodule:: gp_api.utils.multigauss
   :members:
   :undoc-members:
   :show-inheritance:

gp\_api.utils.utils module
--------------------------

.. automodule:: gp_api.utils.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gp_api.utils
   :members:
   :undoc-members:
   :show-inheritance:
