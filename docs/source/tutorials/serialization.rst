Serializing fits
================

Fitting a Gaussian process can be a resource intensive problem.  It is therefore important to be able to save and load our fits to a file.

.. todo:: take example from serialization Jupyter notebook
