gp\_api.kernels package
=======================

Submodules
----------

gp\_api.kernels.compact\_kernel\_evaluate module
------------------------------------------------

.. automodule:: gp_api.kernels.compact_kernel_evaluate
   :members:
   :undoc-members:
   :show-inheritance:

gp\_api.kernels.kernels module
------------------------------

.. automodule:: gp_api.kernels.kernels
   :members:
   :undoc-members:
   :show-inheritance:

gp\_api.kernels.matern\_kernel\_evaluate module
-----------------------------------------------

.. automodule:: gp_api.kernels.matern_kernel_evaluate
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gp_api.kernels
   :members:
   :undoc-members:
   :show-inheritance:
